function observer(){
	var push_to_array_of_property = function(dict, property, el) {
		if (dict[property] === undefined) dict[property] = [el];
		else dict[property].push(el);
	};

	return {
		registered: {},
		register: function(model, el) {
			push_to_array_of_property(this.registered, model, el);
		},

		evaluateRegisteredElement: function(scope) {
			var self = this;
			for (var model in self.registered) {
				Object.defineProperty(scope, model, (function(m, s) {
					return {
							get: function() { return s.values[m]; },
							set: function(val) {
								s.values[m] = val;
								self.onChange(m, val);
							}
						};
					})(model, scope)
				);
			}
		},

		onChange: function(model, val) {
			for (var i = this.registered[model].length - 1; i >= 0; i--) {
				var el = this.registered[model][i];
				var tagName = el.tagName.toUpperCase();
				if (tagName === 'IMG'){
					el.src = val;
				} else if (tagName === 'SELECT'){
					el.value = val;						
				} else {
					el.innerHTML = val;
					el.value = val;
				}
			};
		}
	}
}

var app = (function(scope) {

	var getAllElementsWithAttribute = function (attribute, node) {
		var matchingElements = [];
		var allElements = node ? dom.traverseChildren(node) : document.getElementsByTagName('*');
		return js.filter(allElements, function(el) {
			return el.getAttribute && el.getAttribute(attribute) !== null;
		});
	}

	var bindFunction = function(scope, attr, event, func, callback) {
		getAllElementsWithAttribute(attr, scope.root).forEach(function(el) {
			var f = el.getAttribute(attr);
			el.addEventListener(event, (function(s) {
				return function() {
					eval('app.scopeForName(\"' + s.context + '\").' + f);
					if (callback !== undefined) callback(el);
				}
			})(scope));
			if (func !== undefined) func(el);
		});
	};

	var setElementValueToScopeFunc = function(s, m, el) {
		return function(e) {
			s[(m || (el || e).getAttribute('app-model'))] = (el || e).value;
		};
	};

	var runOnScope = function(scope, controller, data) {
		var obs = new observer();

		getAllElementsWithAttribute(scope.context + '-model', scope.root).forEach(function(el) {
			var model = el.getAttribute(scope.context + '-model');
			obs.register(model, el);
		});

		obs.evaluateRegisteredElement(scope);

		if (data) {
			for (var key in data) {
				scope[key] = data[key];
			}
		}
		controller(scope);

		getAllElementsWithAttribute(scope.context + '-model', scope.root).forEach(function(el) {
			var model = el.getAttribute(scope.context + '-model');
			el.addEventListener('input', setElementValueToScopeFunc(scope, model, el));
			el.addEventListener('change', setElementValueToScopeFunc(scope, model, el));
		});

		bindFunction(scope, scope.context + '-submit', 'submit', function(el) {
			el.setAttribute('onsubmit', 'return false;');
		});
		bindFunction(scope, scope.context + '-click', 'click');
		bindFunction(scope, scope.context + '-change', 'change', undefined, setElementValueToScopeFunc(scope));
	}

	var runOnScopeNextTick = function (scopeObj, data) {
		js.defer(function(){
			runOnScope(scopeObj.scope, scopeObj.controller, data);
		});
	}

	return {

		registeredContext: [],

		register: function(context, controller){
			var newScope = new scope(context)
			var scopeObj = {
				'scope': newScope,
				'controller': controller
			}
			this.registeredContext.push(scopeObj); 
			return scopeObj;
		},

		bind: function(context, controller, data) {
			var scopeObj = this.register(context, controller);
			runOnScopeNextTick(scopeObj, data);
		},

		scopeForName: function(name) {
			for (var i = this.registeredContext.length - 1; i >= 0; i--) {
				if (this.registeredContext[i].scope.context === name) return this.registeredContext[i].scope;
			};
		},

		run: function(){
			for (var i = this.registeredContext.length - 1; i >= 0; i--) {
				var scope = this.registeredContext[i].scope;
				var controller = this.registeredContext[i].controller;
				runOnScope(scope, controller);
			}
		}
	};

})(scope);