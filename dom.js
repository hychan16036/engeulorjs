var dom = (function (d) {
	var exports = {
		createNode: function(tag, attrs, className) {
			var node = d.createElement(tag);
			for (var key in attrs){
				node.setAttribute(key, attrs[key]);
			}
			node.className = (className || '');
			return node;
		},
		removeNodeById :function(id) {
			var el = d.getElementById(id);
			el.parentElement.removeChild(el);
		},
		appendChildren: function(el, children) {
			for (var i = 0; i < children.length; i++) {
				el.appendChild(children[i]);
			};
			return el;
		},
		classes: function(el) {
			return el.className ? el.className.split(' ') : [];
		},
		addClass: function(el, cl) {
			if (exports.classes(el).indexOf(cl) === -1) {
				el.className += ' ' + cl;
			}
		},
		removeClass: function(el, cl) {
			var thisClass = exports.classes(el);
			var idx = thisClass.indexOf(cl);
			if (thisClass.indexOf(cl) !== -1){
				thisClass.splice(idx, 1);
				el.className = thisClass.join(' ');
			}
		},
		traverseChildren: function(el) {
			if (!el || el.childNodes.length == 0) {
				return [];
			}

			var out = [];
			for (var i = 0; i < el.childNodes.length; i++) {
				out = Array.prototype.concat(out, exports.traverseChildren(el.childNodes[i]));
			}

			return Array.prototype.concat(Array.prototype.slice.call(el.childNodes), out);
		}
	};
	return exports;
})(document);