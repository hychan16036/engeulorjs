var http = (function(){

	var objToParam = function(obj) {
		var str = "";
		for (var key in obj) {
			if (str != "") {
					str += "&";
			}
			str += key + '=';
			if (typeof obj[key] === 'object') {
				for (var i = 0; i < obj[key].length; i++) {
					str += encodeURIComponent(obj[key][i]);
					if (i !== obj[key].length - 1){
						str += ',';
					}
				}
			} else {
				str += encodeURIComponent(obj[key]);
			}
		}
		return str;
	}

	var ajax = function(method) {
		return function(target, param, func) {
			var xmlhttp = new XMLHttpRequest();
			var paramStr = objToParam(param);
			console.log(paramStr);
			if (method === 'GET') {
				xmlhttp.open(method, target + '?' + paramStr, true);
				xmlhttp.send();
			} else {
				xmlhttp.open(method, target, true);
				xmlhttp.setRequestHeader("Content-type","application/json");
				xmlhttp.send(JSON.stringify(param));
			}
			xmlhttp.onreadystatechange = function(){
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
					console.log('http result for (' + target + '): ', xmlhttp.responseText);
					if (func) {
						if (xmlhttp.status == 200) func(undefined, xmlhttp.responseText);
						else func(xmlhttp.status, undefined);
					}
				}
			}
		};
	}

	return {
		get: ajax('GET'),
		post: ajax('POST'),
		put: ajax('put'),
		delete: ajax('DELETE')
	};

})();