function scope(context) {

	var getElementWithAttributeOfValue = function(attribute, value) {
		var matchingElements = [];
		var allElements = document.getElementsByTagName('*');
		return js.filter(allElements, function(el) {
			return el.getAttribute(attribute) === value;
		})[0];
	}

	var search = location.search.substring(1);
	var params = {};
	search.split('&').forEach(function(s) {
		var split = s.split('=');
		params[split[0]] = split.length == 1 ? "" : split[1]; 
	});

	return {
		context: context.toLowerCase(),
		root: getElementWithAttributeOfValue('app-context', context),
		values: {},
		url: document.URL,
		stack: document.URL.split('http://')[1].split('?')[0].split('/'),
		params: params
	}
}