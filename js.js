var js = (function (d) {
	return {
		defer: function(func) {
			setTimeout(func, 0);
		},
		map: function(array, func) {
			var out = [];
			for (var i = 0; i < array.length; i++) {
				out.push(func(array[i]));
			}
			return out;
		},
		filter: function(array, predicate) {
			var out = [];
			for (var i = 0; i < array.length; i++) {
				var obj = array[i];
				if (predicate(obj)) out.push(obj);
			}
			return out;
		},
	};
})(document);